from copy import deepcopy

import numpy as np
import h5py
import os
from go import GO

def convert_datapoint_to_game_state(state):
    board = state[:25].reshape((5,5)).tolist()
    prev_board = state[25:50].reshape((5,5)).tolist()
    if state[50]==-1:   action = None
    elif state[50]==25: action = "PASS"
    else: action = (state[50]//5, state[50]%5)
    piece_type = 2-state[51]
    n_move = state[52]
    done = state[53]
    winner = 2-state[54]
    return board, prev_board, action, piece_type, n_move, done, winner

def get_boards_from_state(state):
    board = state[:25].reshape((5, 5)).tolist()
    prev_board = state[25:50].reshape((5, 5)).tolist()
    return board, prev_board

def test_data(file):
    file = file
    fh = h5py.File(file, 'r')
    datasets = fh.keys()
    dataset_keys = []
    for key in datasets:
        dataset_keys.append(key)

    print('file: {}'.format(file))
    print('keys: {}'.format(dataset_keys))

    for dataset in dataset_keys:
        data = fh[dataset][:]

        n = data.shape[0]
        print('dataset: {}, total states: {}'.format(dataset, n))

        consec_pass = 0
        for i in range(n):

            if(np.sum(data[i]))==-55:
                continue

            board, prev_board, action, piece_type, n_move, done, winner = convert_datapoint_to_game_state(data[i])
            if not done:
                go = GO(5)
                go.board = board
                go.previous_board = prev_board
                go.n_move = n_move
                go.X_move = 1 if piece_type==1 else 0

                go = go.next(action)

                next_board, next_prev_board, next_action, next_piece_type, next_n_move, next_done, next_winner = convert_datapoint_to_game_state(data[i])

                assert go.compare_board(go.board, next_board)
                assert go.compare_board(board, next_prev_board)
                assert go.get_piece_type == next_piece_type

                if go.game_end():
                    result = go.judge_winner()
                    assert next_done==1
                    assert next_winner==result

            if i%5000==0:
                print('completed {}/{}'.format(i,n))

if __name__ == '__main__':
    data_dir = 'game_data2'
    files = os.listdir(data_dir)
    for f in files:
        filepath = os.path.join(data_dir, f)
        test_data(filepath)