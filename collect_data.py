import random
from copy import deepcopy
import h5py
import time
import numpy as np
from multiprocessing import Process
import os
from pathlib import Path
from go import GO
from config import Config

num_processes = Config.cores

def get_board(np_array, idx, col,states):
    n = len(states)
    assert n<=25
    for itr in range(n):
        offset = 0
        state = states[itr]
        board = state.board
        prev_board = state.previous_board
        for i in range(5):
            for j in range(5):
                np_array[idx+itr, col+offset] = board[i][j]
                offset+=1

        for i in range(5):
            for j in range(5):
                np_array[idx+itr, col+offset] = prev_board[i][j]
                offset+=1

def get_action(np_array, idx, col,actions):

    n = len(actions)
    assert n<=24

    for itr in range(n):
        action = actions[itr]
        if action=="PASS":
            np_array[idx+itr, col] = 25
        else:
            np_array[idx+itr, col] = action[0]*5+ action[1]

    np_array[idx+n, col] = -1

def get_x_move(np_array, idx, col,n):
    for itr in range(n):
        np_array[idx+itr, col] = (itr+1)%2

def get_done(np_array, idx, col,n):

    np_array[idx+n-1, col] = 1

def get_winner(np_array, idx, col,n, result):

    np_array[idx+n-1, col] = 2-result

def get_n_move(np_array, idx, col,n):

    for itr in range(n):
        np_array[idx+itr, col] = itr

def convert_game2np(np_array, idx, game):
    result, states, actions = game
    n = len(states)
    get_board(np_array, idx, 0, states)  # n x 50
    get_action(np_array, idx, 50, actions)  # n x 1
    get_x_move(np_array, idx, 51, n) # n x 1
    get_n_move(np_array, idx, 52, n) # n x 1
    get_done(np_array, idx, 53, n) # n x 1
    get_winner(np_array, idx, 54, n, result) # n x 1

def append_data_to_file(games, last_save, file_to_save):
    n = len(games)
    game_data = -1*np.ones((25*n,55),dtype=np.int32)

    for i in range(n):
        game = games[i]
        gamp_np = convert_game2np(game_data, i*25, game)

    f = h5py.File(file_to_save, 'a')
    dataset_name = "data_"+str(last_save)
    f.create_dataset(dataset_name, data=game_data, compression="gzip")

def visualize_game(game):
    result, states, actions = game
    n = len(states)
    for i in range(n):
        state = states[i]
        assert state.executed_action==None or state.executed_action == actions[i]
        # state.visualize_board()
        # print(f'player: {state.get_piece_type}')
        # print(f'action: {state.executed_action}')
        # print(f'consecutive_pass: {state.consecutive_pass}')
        # print(f'n_move: {state.n_move}')

    assert result == states[n-1].judge_winner()
    # print(f'winner is {result}')

def collect_data(id, totalGames, saveFrequency, fileToSave):

    print('in process: {}'.format(id))
    TotalGamesToPlay = totalGames
    file_to_save = fileToSave
    save_frequency = saveFrequency

    games = []
    last_save = 0

    start_time = time.time()
    actual_start_time = start_time

    for itrGame in range(TotalGamesToPlay):
        go = GO(5)
        go.init_board(go.size)

        state = []
        state.append(go)
        actions = []
        while True:

            valid_moves = go.find_all_valid_moves()
            action = random.choice(valid_moves)
            actions.append(action)

            go = go.next(action)
            state.append(go)
            if go.game_end():
                result = go.judge_winner()
                games.append((result, state, actions))
                break

        visualize_game(games[itrGame])
        if (itrGame+1)%save_frequency==0:
            append_data_to_file(games, last_save, fileToSave)
            last_save+=1

        if (itrGame+1)%Config.print_frequency==0:
            end_time = time.time()
            total_time = end_time - start_time
            start_time = time.time()
            per_game_time = total_time / save_frequency
            actual_total_time = end_time - actual_start_time
            print('process: {}, games played: {}, total time: {} time per game: {}'.format(id, itrGame + 1,
                                                                                           actual_total_time,
                                                                                           per_game_time))

if __name__ == '__main__':

    gamesPerProcess = Config.gamesPerProcess
    save_frequency = Config.save_frequecy
    processes = []
    Path(Config.data_dir).mkdir(parents=True, exist_ok=True)

    for i in range(num_processes):
        file_to_save = 'file_'+str(i)+'.h5'
        file_path = os.path.join(Config.data_dir, file_to_save)
        p = Process(target=collect_data, args=(i, gamesPerProcess, save_frequency, file_path))
        p.start()
        processes.append(p)

    for p in processes:
        p.join()


